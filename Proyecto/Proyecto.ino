#include <LiquidCrystal_I2C.h> // Libreria LCD_I2C
#include <DHT.h>
#include <DHT_U.h>

#include <DHT.h>
#define DHTPIN 2
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

LiquidCrystal_I2C lcd(0x27,16,2); // si no te sale con esta direccion  puedes usar (0x3f,16,2) || (0x27,16,2)  ||(0x20,16,2) 

// del sensor de lluvia
const int sensorPin = 9;

// del sensor de luminocidad (fotoresistencia)
const int LDRPin = 4;

// Pines para los leds de temperatura
const int tempBLUE = 10;
const int tempGREEN = 8;
const int tempRED = 12;

void setup() {
  lcd.init();
  lcd.backlight();
  lcd.clear();
  pinMode(A0, INPUT);

  // Inicializamos comunicación serie
  Serial.begin(9600);
 
  // Comenzamos el sensor DHT
  dht.begin();

  pinMode(LDRPin, INPUT);
  pinMode(tempBLUE, OUTPUT);
  pinMode(tempGREEN, OUTPUT);
  pinMode(tempRED, OUTPUT);
}

void loop() { 
  lcd.clear();
  int h = dht.readHumidity();
  int t = dht.readTemperature();

  // Comprobamos si ha habido algún error en la lectura
  if (isnan(h) || isnan(t)) {
    Serial.println("Error obteniendo los datos del sensor DHT11");
    return;
  }

  lcd.setCursor(0,0);
  lcd.print("Temperatura:");
  lcd.print(t);
  lcd.print("*C");
  lcd.display();

  lcd.setCursor(0,1);
  lcd.print("Humedad: ");
  lcd.print(h);
  lcd.print("%");
  lcd.display();
  
  //IMPRIMIR EN SERIAL MONITOR
  Serial.print("Temperatura: ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.print("\n");

  //Leds temp
  if(t >= 30){ // Temperatura alta rojo
    digitalWrite(tempRED,HIGH);
    digitalWrite(tempBLUE, LOW);
    digitalWrite(tempGREEN, LOW);
  }else if(t <= 20){ //Temperatura baja azul
    digitalWrite(tempRED,LOW);
    digitalWrite(tempBLUE, HIGH);
    digitalWrite(tempGREEN, LOW);
  }else{  // Temperatura optima verde
    digitalWrite(tempRED,LOW);
    digitalWrite(tempBLUE, LOW);
    digitalWrite(tempGREEN, HIGH);
  }

  delay(2000);
  lcd.clear();
  //SEGUNDA PARTE DE LLUVIA Y NUBLACION

  int value = 0;
  value = digitalRead(sensorPin);

  lcd.setCursor (0,0);
  if(value == LOW){
    lcd.print("Lluvia: SI");
    Serial.print("Lluvia: SI\n");
    tone(11, 4000);
  }else{
    lcd.print("Lluvia: NO");
    Serial.print("Lluvia: NO\n");
    noTone(11);
  }

  //fotoresistencia
  lcd.setCursor(0,1);
  int valueLuz = digitalRead(LDRPin);
  if(valueLuz == HIGH){
    lcd.print("Despejado :D");
    Serial.print("LUZ");
  }else{
    lcd.print("Nublado :C");
    Serial.print("OSCURO");
  }
  
  delay(2000);
}
